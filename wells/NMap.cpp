#include "NMap.h"
#include "Address.h"

namespace PhishNet {
    NMap::NMap() {

    }
    NMap::~NMap() {

    }

    bool NMap::start() {
        this->addOpt(Opt("scan-udp", "-PU ", "ports"));
        this->addOpt(Opt("scan-syn", "-PS ", "ports"));
        this->addOpt(Opt("scan-ack", "-PA ", "ports"));
        this->addOpt(Opt("scan-sctp", "-PY ", "ports"));
        this->addOpt(Opt("dns-server", "--dns-servers ", "address"));
        this->addOpt(Opt("traceroute", "--traceroute", ""));
        this->addOpt(Opt("fast", "-F", ""));
        this->addOpt(Opt("exclude", "--exclude-ports ", "ports"));
        this->addOpt(Opt("consecutive", "-r", ""));
        this->addOpt(Opt("services", "-sV", ""));
        this->addOpt(Opt("services-intensity", "--version-intensity ", "intensity 0-9"));
        this->addOpt(Opt("os", "-O", ""));
        this->addOpt(Opt("timing", "-T", "intensity 0-5"));
        this->addOpt(Opt("scan-delay", "--scan-delay ", "ports"));
        this->addOpt(Opt("fragment", "-f", ""));
        this->addOpt(Opt("spoof-ip", "-S ", "ip"));
        this->addOpt(Opt("interface", "-e ", "interface"));
        this->addOpt(Opt("source-port", "-g ", "port"));
        this->addOpt(Opt("proxies", "--proxies ", "addresses"));
        this->addOpt(Opt("ttl", '--ttl ', 'time'));
        this->addOpt(Opt("spoof-mac", '--spoof-mac ', 'MAC'));
        this->addOpt(Opt("bad-checksum", '--badsum', ''));
    }
    void NMap::terminate() {

    }
    void NMap::update() {

    }

    void NMap::execute(const Address& target) {

    }
}
