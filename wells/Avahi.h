#ifndef PHISHNET_WELL_AVAHI_H
#define PHISHNET_WELL_AVAHI_H
#include "iPhishNet.h"
#include "Well.h"

namespace PhishNet {
    namespace Wells {
        class Avahi : public Well {
        public:
            Avahi();
            virtual ~Avahi();

            virtual bool start() override;
            virtual void terminate() override;

            virtual void update() override;

            ExternalExec *program;
        protected:

        };
    };
}

#endif
