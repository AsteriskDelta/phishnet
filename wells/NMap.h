#ifndef PHISHNET_WELL_NMAP_H
#define PHISHNET_WELL_NMAP_H

namespace PhishNet {
    class Address;
    namespace Wells {
        class NMap : public Well {
        public:
            NMap();
            virtual ~NMap();

            using Well::Opts;

            virtual bool start() override;
            virtual void terminate() override;

            virtual void update() override;

            virtual void execute(const Address& target, const Opts& opts) override;
        };
    }
}

#endif
