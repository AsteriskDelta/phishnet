#include "Avahi.h"
#include <ARKE/ExternalExec.h>
#include "Service.h"
#include <ARKE/StringFNs.h>
namespace PhishNet {
    namespace Wells {
        Avahi::Avahi() {

        }
        Avahi::~Avahi() {

        }

        bool Avahi::start() {
            program = new ExternalExec("avahi-browse -apr 2>&1");
            return true;
        }
        void Avahi::terminate() {

        }

        void Avahi::update() {
            std::string line = "";
            bool hasLine = program->readLine(line);
            if(!hasLine) return;
            std::cout << line << "\n";

            char interface[16];
            char ntype[32];
            char protocol[8];
            char hostname[256];
            char domain[32];
            char dns[64];
            char mac[32];
            char ip[32];
            int port = -1;

            interface[0] = protocol[0] = hostname[0] = domain[0] = dns[0] = mac[0] = 0x0;

            Service *service = new Service();
            int res = sscanf(line.c_str(), "=;%s;%s;%s;%s;%s;%s;%s;%d;", interface, protocol, hostname, ntype, domain, dns, ip, &port);

            auto v = Str::explode(line, ';');
            v.resize(10);

            if(res != 0) {
                service->interface = v[1];//std::string(interface);
                service->protocol = v[2];//std::string(protocol);
                service->hostname = v[3];//std::string(hostname);
                service->domain = v[4];//std::string(domain);
                service->dns = v[6];//std::string(dns);
                service->mac = MAC(v[5]);//MAC(std::string(mac));
                service->address = Address(v[7]);//Address(std::string(ip));
                service->port = atoi(v[8].c_str());
                service->rawMetadata = v[9];
            }

            /*if(res == 0) {
                res = sscanf(line.c_str(), "+;%s;%s;%s;%s;%s;", interface, protocol, dns, hostname, domain);
                if(res != 0) {
                    service->interface = v[1];//std::string(interface);
                    service->protocol = v[2];//std::string(protocol);
                }
            }
            if(res == 0) {
                res = sscanf(line.c_str(), "-;%s;%s;%s;%s;%s;", interface, protocol, dns, hostname, domain);
                if(res != 0) {

                }
            }*/
            if(res == 0) return;

            std::cout << service->str() << "\n";
            delete service;
        }
    }
}
