#include <ARKE/Console.h>
#include <ARKE/Application.h>
#include "Daemon.h"
#include "iPhishNet.h"

using namespace PhishNet;
int background(int status);

int main(int argc, char** argv) {
    _unused(argc, argv);

    Application::BeginInitialization();
    Application::SetName("PhishNet");
    Application::SetCodename("com.d3.phishNet");
    Application::SetCompanyName("Delta III Technologies");
    Application::SetVersion("0.0r1-X");
    Application::SetDataDir("../../data");
    Application::EndInitialization();

    const unsigned short lclPort = 2946;
    Spin::MemberInfo myInfo;
    myInfo.type = Application::GetCodename();
    Spin::MemberID myID(5, lclPort);

    Console::Out("Starting Spinster...");
    Spin::Initialize(myID, myInfo);

    //Spin::Local->schedule(Spin::Priority::Realtime, background, 0);

    Daemon *daemon = new Daemon();
    Daemon::Instance = daemon;
    daemon->run();

    return 0;
}
