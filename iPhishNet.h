#ifndef PHISHNET_GLOBAL_H
#define PHISHNET_GLOBAL_H
#include <NVX/NVX.h>
#include <Spinster/Spinster.h>
#include <ARKE/ARKE.h>

namespace PhishNet {
    using namespace arke;
    using namespace nvx;
}

#endif
