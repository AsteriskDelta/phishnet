#include "Daemon.h"
#include "Avahi.h"

namespace PhishNet {
    Daemon* Daemon::Instance = nullptr;

    Daemon::Daemon() {

    }

    void Daemon::run() {
        Instance = this;
        auto *avahi = new Wells::Avahi();
        wells.push_back(static_cast<Well*>(avahi));

        avahi->start();

        while(true) {
            avahi->update();
            //std::cout << "updating avahi\n";
            usleep(1000);
        }
    }
}
