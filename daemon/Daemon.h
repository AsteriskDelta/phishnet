#ifndef PHISHNET_DAEMON_H
#define PHISHNET_DAEMON_H
#include "iPhishNet.h"
#include <vector>

namespace PhishNet {
    class Well;

    class Daemon {
    public:
        Daemon();

        void run();
        static Daemon *Instance;

        std::vector<Well*> wells;
    protected:

    };
}

#endif
