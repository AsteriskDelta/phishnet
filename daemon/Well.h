#ifndef PHISHNET_WELL_H
#define PHISHNET_WELL_H
#include "iPhishNet.h"

namespace arke {
    class ExternalExec;
}

namespace PhishNet {
    class Address;

    class Well {
    public:

        struct Opt {
            std::string id, cmdline, type
            inline Opt(const std::string i, const std::string c, const std::string t) : id(i), cmdline(c), type(t) {

            }
        };
        struct Opti {
            Opt *opt;
            std::string payload;
        }

        Well();
        virtual ~Well();

        virtual bool start();
        virtual void terminate();

        virtual void update();

        virtual void execute(const Address& o, std::vector<Opti> opts);

        ExternalExec *program;

        inline void addOpt(const Opt& opt) {
            options.push_back(opt);
        }
        inline const std::vector<Opt>& getOpts() const {
            return options;
        }
    protected:
        std::vector<Opt> options;
    };
};

#endif
