#ifndef PHISHNET_ADDR_H
#define PHISHNET_ADDR_H
#include "iPhishNet.h"

namespace PhishNet {
    class Address {
    public:
        Address(const std::string& str = "");
        std::string raw;

        std::string str() const;
    protected:

    };
}

#endif
