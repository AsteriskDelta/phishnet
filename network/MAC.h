#ifndef PHISHNET_MAC_H
#define PHISHNET_MAC_H
#include "iPhishNet.h"

namespace PhishNet {
    class MAC {
    public:
        MAC();
        MAC(const std::string& str);

        void from(const std::string&);

        std::string str() const;
    protected:
        std::string raw;
    };
}

#endif
