#include "Address.h"

namespace PhishNet {
    Address::Address(const std::string& str) : raw(str) {

    }

    std::string Address::str() const {
        return this->raw;
    }
}
