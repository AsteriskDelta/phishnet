#ifndef PHISHNET_SERVICE_H
#define PHISHNET_SERVICE_H
#include "MAC.h"
#include "iPhishNet.h"
#include "Address.h"

namespace PhishNet {
    class Service {
    public:
        Service();

        std::string interface, protocol;
        std::string hostname, domain;
        std::string dns;
        MAC mac;
        Address address;
        unsigned short port;

        std::string rawMetadata;

        std::string str() const;
    protected:

    };
}

#endif
