#include "MAC.h"

namespace PhishNet {
    MAC::MAC() : raw("") {

    }
    MAC::MAC(const std::string& str) {
        this->from(str);
    }

    void MAC::from(const std::string& str) {
        raw = str;
    }

    std::string MAC::str() const {
        return raw;
    }
};
