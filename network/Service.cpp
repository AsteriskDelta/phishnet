#include "Service.h"
#include <sstream>

namespace PhishNet {
    Service::Service() : interface(), protocol(), hostname(), domain(), dns(), mac(), address(), port(0), rawMetadata() {

    }
    std::string Service::str() const {
        std::stringstream ss;
        ss << "Service("<< interface << ":" << protocol << ", " << hostname << "@" << domain << ", " << dns << ", " << mac.str() << ", "<<address.str() <<":" << port <<")";
        return ss.str();
    }
}
